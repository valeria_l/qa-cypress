## Running the tests locally

Clean previous test reports

```$ npm run clean```

Run the tests

```$ npm run test```

Merge Mochawesome test results into one json file

```$ npm run report:merge```

Generate html report to cypress/reports/html/mochawesome-bundle.html file

```$ npm run report:generate```

## Running in Docker container with a single command

```$ docker run -it -v $PWD:/e2e -w /e2e cypress/included:4.5.0 --config baseUrl=http://host.docker.internal:5000```



