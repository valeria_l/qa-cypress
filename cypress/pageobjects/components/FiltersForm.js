export default class FiltersForm {

	static send(name, city) {
		cy.get('#name').clear()
		if (name != null) {
			cy.get('#name').type(name)
		}
		cy.get('#city').clear()
		if (city != null) {
			cy.get('#city').type(city)
		}
		cy.get('#filters button').contains('Submit').click()

	}

	static nameEnter(name) {
		cy.get('#name').clear().type(`${name}{enter}`)
	}

	static cityEnter(city) {
		cy.get('#city').clear().type(`${city}{enter}`)
	}

	static clear() {
		cy.get('#filters button').contains('Clear').click()
	}

	static isName(name) {
		cy.get('#name').should('have.value', name)
	}

	static isCity(city) {
		cy.get('#city').should('have.value', city)
	}

}