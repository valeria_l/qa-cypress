export default class ApplicationsPage {

	static clickRight(name) {
		this.getUserCard(name).find('button').contains('>').click()
		return this
	}

	static clickLeft(name) {
		this.getUserCard(name).find('button').contains('<').click()
		return this
	}

	static getUserCard(name) {
		return cy.get('.CrewMemeber-name').contains(name).closest('.CrewMember-container')
	}

	static getUserCards() {
		return cy.get('.CrewMember-container')
	}

	static getRightArrow(name) {
		return cy.get('.CrewMemeber-name').contains(name).closest('.CrewMember-container').find('button').contains('>')
	}

	static getLeftArrow(name) {
		return cy.get('.CrewMemeber-name').contains(name).closest('.CrewMember-container').find('button').contains('<')
	}

	static getStatus(name) {
		cy.get('.CrewMemeber-name').contains(name).closest('.App-column').find('h2').invoke('text').then((text) => {return text})
	}

	static getHeadingMatch(name, stage) {
		cy.get('.CrewMemeber-name').contains(name).closest('.App-column').find('h2').contains(stage)
		return this
	}

	static getFirstCardNameForStage(stage) {
		return cy.get('h2').contains(stage).siblings().first().find('.CrewMemeber-name div').first().invoke('text')
	}
}