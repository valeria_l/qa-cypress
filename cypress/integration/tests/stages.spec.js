import ApplicationsPage from '../../pageobjects/pages/ApplicationsPage'

describe('Stage transitions', () => {
	['Applied', 'Interviewing'].forEach((stage) => {
		it(`${stage} stage user card has a > button`, () => {
			ApplicationsPage.getFirstCardNameForStage(stage).then((text) => {
				cy.log(`First user card name is ${text}`)
				ApplicationsPage.getRightArrow(text).should('be.visible')
			})
		})
	});

	['Interviewing', 'Hired'].forEach((stage) => {
		it(`${stage} stage user card has a < button`, () => {
			ApplicationsPage.getFirstCardNameForStage(stage).then((text) => {
				cy.log(`First user card name is ${text}`)
				ApplicationsPage.getLeftArrow(text).should('be.visible')
			})
		})
	});

	it('should move Applied > Interviewing < Applied', () => {
		const name = 'danielle moore'
		ApplicationsPage.getHeadingMatch(name, 'Applied')
			.clickRight(name)
			.getHeadingMatch(name, 'Interviewing')
			.clickLeft(name)
			.getHeadingMatch(name, 'Applied')
	})

	it('should move Applied > Interviewing > Hired', () => {
		const name = 'linda ruiz'
		ApplicationsPage.getHeadingMatch(name, 'Applied')
			.clickRight(name)
			.getHeadingMatch(name, 'Interviewing')
			.clickRight(name)
			.getHeadingMatch(name, 'Hired')
	})

	it('should move Hired < Interviewing > Hired', () => {
		const name = 'julia cunningham'
		ApplicationsPage.getHeadingMatch(name, 'Hired')
			.clickLeft(name)
			.getHeadingMatch(name, 'Interviewing')
			.clickRight(name)
			.getHeadingMatch(name, 'Hired')
	})

	it('should move Hired < Interviewing < Applied', () => {
		const name = 'julia cunningham'
		ApplicationsPage.getHeadingMatch(name, 'Hired')
			.clickLeft(name)
			.getHeadingMatch(name, 'Interviewing')
			.clickLeft(name)
			.getHeadingMatch(name, 'Applied')
	})

	beforeEach(() => {
		cy.visit('/')
		ApplicationsPage.clickRight('lloyd gonzalez')
		// move 'lloyd gonzalez' from 'Applied' to 'Interviewing' to have at least one card for every stage
	})
})
