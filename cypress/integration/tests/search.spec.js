import FiltersForm from '../../pageobjects/components/FiltersForm'
import ApplicationsPage from '../../pageobjects/pages/ApplicationsPage'

function testSearch (name, city, expectedNames) {
	FiltersForm.send(name, city);
	ApplicationsPage.getUserCards().should('have.length', expectedNames.length)
	expectedNames.forEach((name) => {
		ApplicationsPage.getUserCard(name).should('be.visible')
	})
}

describe('Search', () => {
	it('should display all cards when we submit an empty form', () => {
		testSearch(null, null, ['emma stewart', 'danielle moore', 'linda ruiz', 'lloyd gonzalez', 'julia cunningham'])
	})

	it('should search by first name', () => {
		testSearch('emma', null, ['emma stewart'])
	})

	it('should search by first name (partial)', () => {
		testSearch('aniell', null, ['danielle moore'])
	})

	it('should search by last name', () => {
		testSearch('ruiz', null, ['linda ruiz'])
	});

	['gonz', 'onzalez', 'zal'].forEach((lastNamePartial) => {
		it('should search by last name (partial)', () => {
			testSearch(lastNamePartial, null, ['lloyd gonzalez'])
		})
	})

	it('should not search by full name', () => {
		testSearch('julia cunningham', null, [])
	})

	it('should search by city', () => {
		testSearch(null, 'worcester', ['emma stewart'])
	});

	['car', 'rdi', 'cardiff'].forEach((cityPartial) => {
		it('should search by city (partial)', () => {
			testSearch(null, cityPartial, ['danielle moore'])
		})
	})

	it('should search name by one letter', () => {
		testSearch('n', null, ['danielle moore', 'linda ruiz', 'lloyd gonzalez', 'julia cunningham'])
	})

	it('should search city by one letter', () => {
		testSearch(null, 'o', ['emma stewart', 'linda ruiz', 'lloyd gonzalez'])
	})

	it('should search by name and city', () => {
		testSearch('linda', 'liverpool', ['linda ruiz'])
	}); 

	[' lloyd', 'lloyd '].forEach((nameWithWhitespace, index) => {
		it(`should not search by name with whitespace characters (${index+1})`, () => {
			testSearch(nameWithWhitespace, null, [])
		});
	});

	[' hereford', 'hereford '].forEach((cityWithWhitespace) => {
		it('should not search by city with whitespace characters', () => {
			FiltersForm.send(null, cityWithWhitespace)
			ApplicationsPage.getUserCards().should('have.length', 0)
		})
	})

	it('should submit name with enter', () => {
		const name = 'julia'
		FiltersForm.nameEnter(name)
		const expectedName = 'julia cunningham'
		ApplicationsPage.getUserCards().should('have.length', 1)
		ApplicationsPage.getUserCard(expectedName).should('be.visible')
	})

	it('should submit city with enter', () => {
		const city = 'worcester'
		FiltersForm.cityEnter(city)
		const expectedName = 'emma stewart'
		ApplicationsPage.getUserCards().should('have.length', 1)
		ApplicationsPage.getUserCard(expectedName).should('be.visible')
	})

	it('should clear the search results after clicking on Clear', () => {
		const name = 'danielle'
		const city = 'cardiff'
		FiltersForm.send(name, city)
		FiltersForm.clear()
		const expectedNames = ['emma stewart', 'danielle moore', 'linda ruiz', 'lloyd gonzalez', 'julia cunningham']
		ApplicationsPage.getUserCards().should('have.length', 5)
		expectedNames.forEach((name) => {
			ApplicationsPage.getUserCard(name).should('be.visible')
		})
	})

	it('should not clear form fields after clicking on Clear', () => {
		const name = 'danielle'
		const city = 'cardiff'
		FiltersForm.send(name, city)
		FiltersForm.clear()
		FiltersForm.isName(name)
		FiltersForm.isCity(city)
	})

	it('should not clear form fields after clicking on Submit', () => {
		const name = 'danielle'
		const city = 'cardiff'
		FiltersForm.send(name, city)
		FiltersForm.isName(name)
		FiltersForm.isCity(city)
	})

	it('should show search results after page reload', () => {
		const name = 'julia'
		FiltersForm.nameEnter(name)
		const expectedName = 'julia cunningham'
		cy.reload()
		ApplicationsPage.getUserCards().should('have.length', 1)
		ApplicationsPage.getUserCard(expectedName).should('be.visible')
	})
	it('search by name should be case-sensitive', () => {
		testSearch('eMma', null, [])
	})

	it('search by city should be case-sensitive', () => {
		testSearch(null, 'LIVERPOOL', [])
	})

	it('should return multiple results for multiple matching cards', () => {
		testSearch('le', null, ['danielle moore', 'lloyd gonzalez'])
	})

	beforeEach(() => {
		cy.visit('/')
		ApplicationsPage.clickRight('lloyd gonzalez')
		// move 'lloyd gonzalez' from 'Applied' to 'Interviewing' to have at least one card for every stage
	})
})